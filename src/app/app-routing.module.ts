import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MessageComponent } from './message/message.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginGuard } from './guards/login.guard';
import { CustomLoadingStrategyService } from './custom-module-loading-strategy/custom-loading-strategy.service';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent},
  {
    path: 'products', canActivate: [LoginGuard], data: {preload : false}, //canLoad: [LoginGuard]
    loadChildren: () => import('./product-features/product-features.module').then(m => m.ProductFeaturesModule)
  },
  { path: 'message', component: MessageComponent, outlet: 'popup' },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy', preloadingStrategy: CustomLoadingStrategyService })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
