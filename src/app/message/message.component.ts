import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  message: string = 'Hello World!'

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  close(){
    this.router.navigate([{outlets: {popup: null}}])
  }
}
