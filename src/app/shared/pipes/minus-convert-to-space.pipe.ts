import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minusConvertToSpace'
})
export class MinusConvertToSpacePipe implements PipeTransform {

  transform(value: string, args: string): string {
    return value.replace(args, ' ');
  }

}
