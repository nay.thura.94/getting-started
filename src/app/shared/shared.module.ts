import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StartComponent } from './start/start.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MinusConvertToSpacePipe } from './pipes/minus-convert-to-space.pipe';



@NgModule({
  declarations: [StartComponent, MinusConvertToSpacePipe],
  imports: [
    CommonModule
  ],
  exports: [StartComponent, CommonModule,FormsModule,ReactiveFormsModule, MinusConvertToSpacePipe]
})
export class ShareModule { }
