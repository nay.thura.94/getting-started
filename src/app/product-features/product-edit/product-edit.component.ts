import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../../models/product';
import { ProductServices } from 'src/app/services/product.service';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  product: Product
  productForm: FormGroup

  constructor(private route: ActivatedRoute, private productService: ProductServices, private fb: FormBuilder,
    private router: Router) { }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.productService.findById(id).subscribe(product => {
      this.product = product
    })
  }

  buildProductEditForm(){
    this.productForm = this.fb.group({
      productName: [this.product?.productName, [Validators.required]],
      productCode: [this.product?.productCode, [Validators.required]],
      description: [this.product?.description, [Validators.required]],
    })
  }

  onSubmit(){
    const {productName, productCode, description} = this.productForm.value;
    this.product = {...this.product, productName: productName, productCode: productCode, description: description}
    this.productService.updateProduct(this.product)

  }
}
