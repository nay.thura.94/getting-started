import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../../models/product';
import { ProductServices } from 'src/app/services/product.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit {

  product: Product
  productForm: FormGroup
  private orginalProduct: Product;
  private currentProduct: Product

  constructor(private route: ActivatedRoute, private productService: ProductServices, private fb: FormBuilder,
    private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.route.parent.data.subscribe(data => {
      this.product = data['resolveProductData'];
      this.orginalProduct = data['resolveProductData'];
      console.log('original product => ', this.orginalProduct)
      this.buildProductEditForm();
    })

    this.productForm.valueChanges.subscribe(product => {
      const {productName, productCode, description} = product
      this.currentProduct = {...this.orginalProduct, productName: productName, productCode: productCode, description: description}
      console.log('original product => ', this.orginalProduct)
      console.log('current product => ', this.currentProduct)
      this.authService.isDirtyInInfoComp = JSON.stringify(this.orginalProduct) !== JSON.stringify(this.currentProduct)
      console.log('isDirty in infor component => ', this.authService.isDirtyInInfoComp)
    })
  }

  buildProductEditForm(){
    this.productForm = this.fb.group({
      productName: [this.product?.productName, [Validators.required]],
      productCode: [this.product?.productCode, [Validators.required]],
      description: [this.product?.description, [Validators.required]],
    })
  }

  onSubmit(){
    const {productName, productCode, description} = this.productForm.value;
    this.product = {...this.product, productName: productName, productCode: productCode, description: description}
    this.productService.updateProduct(this.product)
    this.router.navigateByUrl('/products')
  }

}
