import { ProductServices } from './../../services/product.service';
import { Product } from './../../models/product';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  pageTitle: string = 'Product Detail'
  product: Product
  showImage: boolean = false

  constructor(private route: ActivatedRoute, private productService: ProductServices, private router: Router) { }

  ngOnInit() {
    this.showImage = this.route.snapshot.queryParamMap.get('showImage') === 'true';
    this.productService.findById(+this.route.snapshot.paramMap.get('id')).subscribe({
      next: (product) => this.product = product
    })
  }
}
