import { ProductServices } from './../../services/product.service';
import { Product } from './../../models/product';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  pageTitle: string = 'My Products';
  errorMessage: string;
  products: Product[];
  showImage: boolean = false
  filterText: string
  filtereProducts: Product[];

  constructor(private productService: ProductServices, private rote: ActivatedRoute) { }

  ngOnInit(): void {
    this.filterText = this.rote.snapshot.queryParamMap.get('filterText') || '';
    this.showImage = (this.rote.snapshot.queryParamMap.get('showImage') === 'true');
    this.productService.getProducts().subscribe({
      next: (products) => {
        this.products = products;
        this.modelOnChange(this.filterText);
      },
      error: (error) => this.errorMessage = error
    })
  }

  toggleImage(): void {
    this.showImage = !this.showImage
  }

  modelOnChange(filterText: string): void {
    if (filterText)
      this.filtereProducts = this.products.filter(product => product.productName.toLowerCase().indexOf(filterText) !== -1)
    else this.filtereProducts = this.products
  }

  starClicked(value: string) {
  }
}
