import { FormsModule } from '@angular/forms';
import { ShareModule } from '../shared/shared.module';
import { ProductFeaturesRoutingModule } from './product-features-routing.module';
import { ProductListComponent } from './product-list/product-list.component';
import { NgModule } from '@angular/core';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { ProductDataService } from '../services/product-data.service';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductInfoComponent } from './product-info/product-info.component';
import { ProductTagComponent } from './product-tag/product-tag.component';



@NgModule({
  declarations: [ProductDetailComponent, ProductListComponent, ProductEditComponent, ProductInfoComponent, ProductTagComponent],
  imports: [
    ProductFeaturesRoutingModule,FormsModule,
    ShareModule
  ]
})
export class ProductFeaturesModule { }
