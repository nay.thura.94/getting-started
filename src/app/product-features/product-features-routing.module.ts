import { ProductDetailGuardGuard } from './../guards/product-detail-guard.guard';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductListComponent } from './product-list/product-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductInfoComponent } from './product-info/product-info.component';
import { ProductTagComponent } from './product-tag/product-tag.component';
import { ProductResolverService } from '../services/product-resolver.service';
import { LoginGuard } from '../guards/login.guard';
import { ProductEditGuard } from '../guards/product-edit.guard';


const routes: Routes = [

    { path: '', component: ProductListComponent },
    { path: ':id', component: ProductDetailComponent, canActivate: [ProductDetailGuardGuard] },
    {path: ':id/edit', component: ProductEditComponent, resolve: {resolveProductData: ProductResolverService},
    canDeactivate: [ProductEditGuard],
    children:[
      {path: 'info', component: ProductInfoComponent},
      {path: 'tag', component: ProductTagComponent},
      {path: '', redirectTo: 'info', pathMatch: 'full'}
    ]}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductFeaturesRoutingModule { }
