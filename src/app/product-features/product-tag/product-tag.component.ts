import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/models/product';
import { ProductServices } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-tag',
  templateUrl: './product-tag.component.html',
  styleUrls: ['./product-tag.component.css']
})
export class ProductTagComponent implements OnInit {

  product: Product
  productForm: FormGroup

  constructor(private route: ActivatedRoute, private productService: ProductServices, private fb: FormBuilder,
    private router: Router) { }

  ngOnInit(): void {
    this.route.parent.data.subscribe(data => {
      this.product = data['resolveProductData'];
      this.buildProductEditForm();
    })
  }

  buildProductEditForm(){
    this.productForm = this.fb.group({
      productName: [this.product?.productName, [Validators.required]],
      productCode: [this.product?.productCode, [Validators.required]],
      description: [this.product?.description, [Validators.required]],
    })
  }

  onSubmit(){
    const {productName, productCode, description} = this.productForm.value;
    this.product = {...this.product, productName: productName, productCode: productCode, description: description}
    this.productService.updateProduct(this.product)
    this.router.navigateByUrl('/products')
  }

}
