import { ProductApiService } from 'product-api';
import { Product } from './../models/product';
import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators'

@Injectable({
    providedIn: 'root'
})

export class ProductServices {

    private productUrl: string = 'api'
    private header : HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

    constructor(private http: HttpClient) { }

    getProducts(): Observable<Product[]> {
        return this.http.get<Product[]>(`${this.productUrl}/products`)
            .pipe(
                catchError(this.handleError)
            )
    }

    findById(id: number): Observable<Product> {
        return this.http.get<Product>(`${this.productUrl}/products/${id}`)
    }

    updateProduct(product: Product){
      return this.http.put<Product>(`${this.productUrl}/products/${product.id}`,product, {headers: this.header})
      .pipe(
        tap(product => console.log('product in product service => ', product)),
        catchError(this.handleError)
      )
    }

    private handleError(err: HttpErrorResponse): Observable<never> {
        let errorMessage = '';
        if (err.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            errorMessage = `An error occured: ${err.error.message}`
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage)
        return throwError(errorMessage)
    }
}
