import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { ProductInfoComponent } from '../product-features/product-info/product-info.component';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ProductEditGuard implements CanDeactivate<ProductInfoComponent> {

  constructor(private authService: AuthService){}

  canDeactivate(component: ProductInfoComponent, currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
      console.log('isDirty in ProductEditGuard service => ', this.authService.isDirtyInInfoComp)
    if(this.authService.isDirtyInInfoComp) {
      return confirm('If navigate away and lose all changes');
    }
    return true
  }

}
