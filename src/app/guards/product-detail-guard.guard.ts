import { map } from 'rxjs/operators';
import { Product } from './../models/product';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ProductServices } from '../services/product.service';

@Injectable({
  providedIn: 'root'
})
export class ProductDetailGuardGuard implements CanActivate {

  constructor(private router: Router, private productService: ProductServices) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let id = +next.url[1]
    if(!isNaN(id)){
      return this.productService.findById(id)
      .pipe(
        map(product => {
          if (product === undefined) {
            this.router.navigateByUrl('/products')
            return false;
          }
          return true;
        })
      )
    }else{
      this.router.navigateByUrl('/products')
            return false;
    }
  }
}
