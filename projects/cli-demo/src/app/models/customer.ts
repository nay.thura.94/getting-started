export class Customer {
  firstName: string
  lastName: string
  email: string
  confirmEmail: string
  phone: string
  rating: number
}
