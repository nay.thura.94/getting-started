import { Customer } from './models/customer';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'cli-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  customer: Customer =  new Customer()
  customerForm: FormGroup

  constructor(private fb: FormBuilder){}

  ngOnInit(): void {
    this.customerForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      emailGroup: this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        confirmEmail: ['', Validators.required],
      }, {validator: this.compareEmail}),
      phone: [''],
      notification: ['email'],
      rating: [ null, this.validateRatingWithParameter(1, 5)]
    })
  }

  validateRatingWithParameter(min: number, max: number){
    return (rating: AbstractControl): {[key: string] : boolean} | null => {
      if(rating.value != null && ( isNaN(rating.value) || rating.value < min || rating.value > max)) return {'range': true};
      else return
    }
  }

  compareEmail(emailGroup: AbstractControl): {[key : string]: boolean} | null{
    let email = emailGroup.get('email');
    let confirmEmail = emailGroup.get('confirmEmail');
    if(email.pristine || confirmEmail.pristine) return null;
    if(email.value === confirmEmail.value) return null;
    else return {match : true};
  }

  onSubmit(){
    console.log('customer form value => ', this.customerForm.controls)
  }

  fillSimpleFormData(){
    this.customerForm.patchValue({
      firstName: "Ed",
      lastName: "Sheeran",
      emailGroup: {email: "edsherran@gmail.com",confirmEmail: "edsherran@gmail.com",},
      phone: "09999454444",
      rating: 1
    })
  }

  setValiatorBasOnNotification(notification: string){
    console.log('click radio => ', notification)
    const phoneControl = this.customerForm.get('phone');
    if(notification === 'text') phoneControl.setValidators(Validators.required);
    else phoneControl.clearValidators();
    phoneControl.updateValueAndValidity();
  }
}
