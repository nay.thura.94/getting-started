/*
 * Public API Surface of product-api
 */

export * from './lib/product-api.service';
export * from './lib/product-api.component';
export * from './lib/product-api.module';
