import { NgModule } from '@angular/core';
import { ProductApiComponent } from './product-api.component';



@NgModule({
  declarations: [ProductApiComponent],
  imports: [
  ],
  exports: [ProductApiComponent]
})
export class ProductApiModule { }
