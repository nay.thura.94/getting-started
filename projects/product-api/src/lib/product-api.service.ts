import { Product } from './models/product';
import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ProductApiService {

  private productUrl: string = '../products.json'
  private products$: Observable<Product[]> = of([{
    "id": 1,
    "productName": "Leaf Rake",
    "productCode": "GDN-0011",
    "releaseDate": "March 19, 2019",
    "description": "Leaf rake with 48-inch wooden handle.",
    "price": 19.95,
    "starRating": 3.2,
    "imageUrl": "assets/images/leaf_rake.png"
},
{
    "id": 2,
    "productName": "Garden Cart",
    "productCode": "GDN-0023",
    "releaseDate": "March 18, 2019",
    "description": "15 gallon capacity rolling garden cart",
    "price": 32.99,
    "starRating": 4.2,
    "imageUrl": "assets/images/garden_cart.png"
},
{
    "id": 5,
    "productName": "Hammer",
    "productCode": "TBX-0048",
    "releaseDate": "May 21, 2019",
    "description": "Curved claw steel hammer",
    "price": 8.9,
    "starRating": 4.8,
    "imageUrl": "assets/images/hammer.png"
},
{
    "id": 8,
    "productName": "Saw",
    "productCode": "TBX-0022",
    "releaseDate": "May 15, 2019",
    "description": "15-inch steel blade hand saw",
    "price": 11.55,
    "starRating": 3.7,
    "imageUrl": "assets/images/saw.png"
},
{
    "id": 10,
    "productName": "Video Game Controller",
    "productCode": "GMG-0042",
    "releaseDate": "October 15, 2018",
    "description": "Standard two-button video game controller",
    "price": 35.95,
    "starRating": 4.6,
    "imageUrl": "assets/images/xbox-controller.png"
},
{
  "id": 11,
  "productName": "Video Game Controller",
  "productCode": "GMG-0042",
  "releaseDate": "October 15, 2018",
  "description": "Standard two-button video game controller",
  "price": 35.95,
  "starRating": 4.6,
  "imageUrl": "assets/images/xbox-controller.png"
}
])

    constructor(private http: HttpClient) { }

    productLog(message: string){
      console.log('product log => ', message)
    }

    getProducts(): Observable<Product[]> {
        return this.products$
    }

    findById(id: number): Observable<Product> {
        return this.getProducts()
            .pipe(
                map(products => products.find(product => product.id === id))
            )
    }

    private handleError(err: HttpErrorResponse): Observable<never> {
        let errorMessage = '';
        if (err.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            errorMessage = `An error occured: ${err.error.message}`
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage)
        return throwError(errorMessage)
    }
}
